<?php

require __DIR__ . '/bootstrap/config.php';

return [
	'paths' => [
		'migrations' => __DIR__ . '/database/migrations',
		'seeds'      => __DIR__ . '/database/seeds',
	],
	
	'migration_base_class' => 'App\Database\Migrations\Migration',
	
	'templates' => [
		'file' => 'app/Database/Migrations/MigrationStub.php',
	],
	
	'environments' => [
		'default_migrations_table' => 'migrations',
		
		'default' => [
			'adapter' => $db_settings['driver'],
			'host'    => $db_settings['host'],
			'port'    => $db_settings['port'],
			'name'    => $db_settings['database'],
			'user'    => $db_settings['username'],
			'pass'    => $db_settings['password'],
		]
	]
];