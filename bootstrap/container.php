<?php

$container['flash'] = function ( $container ) {
	return new \Slim\Flash\Messages;
};

$container['view'] = function ( $container ) {
	$view = new \Slim\Views\Twig( __DIR__ . '/../app/Views/', [
		'cache' => $container->settings['views']['cache']
	] );
	
	$basePath = rtrim( str_ireplace( 'index.php', '', $container['request']->getUri()->getBasePath() ), '/' );
	$view->addExtension( new Slim\Views\TwigExtension( $container['router'], $basePath ) );
	
	$view->getEnvironment()->addGlobal( 'flash', $container->flash );
	
	return $view;
};

$container['validator'] = function ( $container ) {
	return new \App\Validation\Validator;
};

$container['csrf'] = function ( $container ) {
	//argument 2 must be passed by reference, so 'null' cannot be passed in directly
	$storage = null;
	
	return new \Slim\Csrf\Guard( 'csrf', $storage, null, 200, 16, true );
};
