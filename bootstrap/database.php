<?php

use Illuminate\Database\Capsule\Manager as Capsule;

$capsule = new Capsule;
$capsule->addConnection( $db_settings );
$capsule->setAsGlobal();
$capsule->bootEloquent();

//$platform = Capsule::schema()->getConnection()->getDoctrineSchemaManager()->getDatabasePlatform();
//$platform->registerDoctrineTypeMapping( 'enum', 'string' );

$container['db'] = function ( $container ) use ( $capsule ) {
	return $capsule;
};

$container['schema'] = function ( $container ) use ( $capsule ) {
	return Capsule::schema();
};

$migration = new Phinx\Wrapper\TextWrapper( new Phinx\Console\PhinxApplication(), ['parser' => 'php', 'configuration' => __DIR__ . '/../phinx.php']);
$migration->getMigrate('default');
/*
 * Uncomment this line to run a seed on the users table.
 * However, beware that because the email column is set
 * to 'unique', subsequent requests with this line in
 * place will yield an error. You should implement some
 * sort of logic as to when the seed should be run, if
 * it should be run at all.
 */

//$migration->getSeed('default');