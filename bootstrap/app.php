<?php

// Include the composer autoload ile
require_once __DIR__ . '/../vendor/autoload.php';

// Start the php session
session_start();

// Attach the custom rules to the validator
Respect\Validation\Validator::with( 'App\\Validation\\Rules\\' );

// Include the configuration settings or the slim app
require_once __DIR__ . '/config.php';

// Instantiate the slim app
$app = new Slim\App( $settings );

// Extract the app's container
$container = $app->getContainer();

// Initialize the database and eloquent
require_once __DIR__ . '/database.php';

// Configure the app's container
require_once __DIR__ . '/container.php';

// Include the app's middleware
require_once __DIR__ . '/middleware.php';

// Include the app's routes
require_once __DIR__ . '/routes.php';
