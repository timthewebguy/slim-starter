<?php

try {
	( new Dotenv\Dotenv( __DIR__ . '/../' ) )->load();
} catch ( Dotenv\Exception\InvalidPathException $e ) {
	//
}

$db_settings = [
	'driver'   => getenv( 'DB_DRIVER' ),
	'host'     => getenv( 'DB_HOST' ),
	'port'     => getenv( 'DB_PORT' ),
	'database' => getenv( 'DB_NAME' ),
	'username' => getenv( 'DB_USERNAME' ),
	'password' => getenv( 'DB_PASSWORD' ),
	'charset'  => getenv( 'DB_CHARSET' ),
	'prefix'   => getenv( 'DB_PREFIX' ),
];

$app_settings = [
	'name' => getenv( 'APP_NAME' )
];

$view_settings = [
	'cache' => false, //getenv( 'VIEW_CACHE_DISABLED' ) === '' ? false : __DIR__ . '/../storage/views'
];
$settings = [
	'settings' => [
		'displayErrorDetails'               => getenv( 'APP_DEBUG' ) === 'true',
		'determineRouteBeforeAppMiddleware' => true,
		'addContentLengthHeader'            => false,
		'db'                                => $db_settings,
		'app'                               => $app_settings,
		'views'                             => $view_settings,
	],
];
