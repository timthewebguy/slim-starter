FROM php:7.0-apache

RUN docker-php-ext-install pdo_mysql

RUN a2enmod rewrite

COPY ./php.ini /usr/local/etc/php/php.ini

COPY ./ /var/www/html/
COPY ./000-default.conf /etc/apache2/sites-enabled/000-default.conf

