# Slim Starter

## What's Inside
```
|-- slim-starter
    |-- app
        |-- Controllers
            |-- Controller.php
            |-- HomeController.php
        |-- Database
        	|-- Migrations
        	    |-- Migration.php
        	    |-- MigrationStub.php
        |-- Middleware
            |-- CsrfViewMiddleware.php
            |-- Middleware.php
            |-- ValidationMiddleware.php
        |-- Models
            |-- Model.php
            |-- User.php
        |-- Validation
            |-- Exceptions
                |-- TestRuleException.php
            |-- Rules
                |-- TestRule.php
            |-- Validator.php
        |-- Views
            |-- templates
                |-- app.twig
            |-- web
                |-- home
                    |-- index.twig
    |-- bootstrap
        |-- app.php
        |-- config.php
        |-- container.php
        |-- database.php
        |-- middleware.php
        |-- routes.php
    |-- database
        |-- migrations
            |-- [...]_test_migration.php
        |-- seeds
            |-- TestSeed.php
    |-- public
        |-- .htaccess
        |-- index.php
    |-- .env
    |-- phinx.php
```

## Development

1. Install Composer dependencies:
   
   `composer install`
   
2. Install node dependencies: 

   `yarn install` or `npm install`
    
3. Start the docker containers

   `docker-compose up`
   
   
## Included Packages

- [Slim Php ^3.8](https://www.slimframework.com/)
- [Slim Twig View ^2.2](https://github.com/slimphp/Twig-View)
- [Eloquent ORM ^5.2](https://github.com/slimphp/Twig-View)
- [Respect Validation ^1.0](https://github.com/Respect/Validation)
- [Slim CSRF ^0.8](https://github.com/slimphp/Slim-Csrf)
- [Slim Flash ^0.1](https://github.com/slimphp/Slim-Flash)
- [Phinx ^0.8.1](https://phinx.org/)
- [Symphony VarDumper ^3.2](https://symfony.com/doc/current/components/var_dumper.html) (Dev) 