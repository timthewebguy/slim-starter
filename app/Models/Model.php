<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as M;

class Model extends M {
	
	/**
	 * @param $model Name of model (including namespace after \Synapse\Modules\)
	 * @param $relationType type of relationship (belongsToMany, hasOne, etc)
	 * @param null $pivot array of columns in a pivot table for many to many relationships
	 * @param string $foreignKey foreign key to be used in the relationship
	 *
	 * @return mixed relationship
	 */
	public function getRelationship ( $model, $relationType, $pivot = null, $foreignKey = '' ) {
		if ( $pivot != null ) {
			if ( $foreignKey != '' ) {
				return $this->$relationType( $model, $foreignKey )->withPivot( $pivot );
			} else {
				return $this->$relationType( $model )->withPivot( $pivot );
			}
			
		} else {
			if ( $foreignKey != '' ) {
				return $this->$relationType( $model, $foreignKey );
			} else {
				return $this->$relationType( $model );
			}
		}
	}
	
}