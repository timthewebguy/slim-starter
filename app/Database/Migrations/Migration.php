<?php

namespace App\Database\Migrations;

use Illuminate\Database\Capsule\Manager;
use Phinx\Migration\AbstractMigration;

class Migration extends AbstractMigration {

	protected $schema;
	public function init() {
		$manager = new Manager();
		$manager->addConnection([
			'driver'    => getenv( 'DB_DRIVER' ),
			'host'      => getenv( 'DB_HOST' ),
			'port'      => getenv( 'DB_PORT' ),
			'database'  => getenv( 'DB_DATABASE' ),
			'username'  => getenv( 'DB_USERNAME' ),
			'password'  => getenv( 'DB_PASSWORD' ),
			'charset'   => getenv( 'DB_CHARSET' ),
			'collation' => getenv( 'DB_COLLATION' ),
			'prefix'    => getenv( 'DB_PREFIX' ),
		]);
		$this->schema = $manager->schema();
	}
}