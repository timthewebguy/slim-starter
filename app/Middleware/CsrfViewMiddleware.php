<?php

namespace App\Middleware;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class CsrfViewMiddleware extends Middleware {
	public function __invoke ( Request $request, Response $response, $next ) {
		
		$this->c->view->getEnvironment()->addGlobal( 'csrf', [
			'field' => '
				<input type="hidden" name="' . $this->c->csrf->getTokenNameKey() . '" value="' . $this->c->csrf->getTokenName() . '">
				<input type="hidden" name="' . $this->c->csrf->getTokenValueKey() . '" value="' . $this->c->csrf->getTokenValue() . '">
			',
		] );
		$response = $next( $request, $response );
		
		return $response;
	}
}
