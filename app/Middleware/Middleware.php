<?php

namespace App\Middleware;

abstract class Middleware {
	protected $c;
	
	public function __construct ( $container ) {
		$this->c = $container;
	}
}
