<?php

namespace App\Middleware;

use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

class ValidationMiddleware extends Middleware {
	public function __invoke ( Request $request, Response $response, $next ) {
		if ( isset( $_SESSION['errors'] ) ) {
			$this->c->view->getEnvironment()->addGlobal( 'errors', $_SESSION['errors'] );
			unset( $_SESSION['errors'] );
		}
		
		if(isset($_SESSION['old'])) {
			$this->c->view->getEnvironment()->addGlobal( 'old', $_SESSION['old'] );
		}
		$_SESSION['old'] = $request->getParams();
		
		return $next( $request, $response );
	}
}
