<?php

namespace App\Validation\Rules;

use Respect\Validation\Rules\AbstractRule;

class TestRule extends AbstractRule {
	protected $value;
	
	public function __construct ( $value ) {
		$this->vlaue = $value;
	}
	
	public function validate ( $input ) {
		return $input === $this->value;
	}
}
