<?php

namespace App\Validation;

use Respect\Validation\Validator as Respect;
use Respect\Validation\Exceptions\NestedValidationException;

class Validator {
	protected $errors;
	
	/**
	 * Validates a set of rules against a set of inputs
	 *
	 * @param Request $request Slim Request Variable
	 * @param array $rules List of rules paired with their value names
	 *
	 * @return Validator
	 */
	public function Validate ( $request, array $rules ) {
		foreach ( $rules as $field => $rule ) {
			try {
				$rule->setName( ucwords( preg_replace( '/_/', ' ', $field ) ) )->assert( $request->getParam( $field ) );
			} catch ( NestedValidationException $e ) {
				$this->errors[ $field ] = $e->getMessages();
			}
		}
		$_SESSION['errors'] = $this->errors;
		
		return $this;
	}
	
	public function addError ( $field, $message ) {
		$this->errors[ $field ] = [ $message ];
		$_SESSION['errors']     = $this->errors;
	}
	
	/**
	 * checks to see if the validation generated any errors
	 * @return boolean
	 */
	public function failed () {
		return ! empty( $this->errors );
	}
}
